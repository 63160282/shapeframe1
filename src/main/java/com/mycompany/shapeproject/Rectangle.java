/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject;

/**
 *
 * @author User
 */
public class Rectangle extends Shape{
    private double W;
    private double L;

    public Rectangle(double W, double L) {
        super("Rectangle");
        this.W = W;
        this.L = L;
    }

    public double getW() {
        return W;
    }

    public void setW(double W) {
        this.W = W;
    }

    public double getL() {
        return L;
    }

    public void setL(double L) {
        this.L = L;
    }
    

    @Override
    public double calArea() {
        return W*L;
    }

    @Override
    public double calPerimter() {
        return 2*(W+L);
    }
    
    
    
}
