/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject;

/**
 *
 * @author User
 */
public class Triangle extends Shape {

    private double side;

    public Triangle(double side) {
        super("Triangle");
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    /**
     *
     * @return
     */
    @Override
    public double calArea() {
        return Math.sqrt(3) / 4 * Math.pow(side, 2);
    }

    @Override
    public double calPerimter() {
        return side * 3;
    }

}
