/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author User
 */
public class RectangleFrame2 extends JFrame{
    JLabel lblh;
    JLabel lbll;
    JTextField txth;
    JTextField txtl;
    JButton btnCalculate;
    JLabel lblResult;
  public RectangleFrame2()  {
        super("Rectangle");
        this.setSize(400 ,300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblh = new JLabel("Wide: ", JLabel.TRAILING);
        lblh.setSize(50, 20);
        lblh.setLocation(5, 5);
        lblh.setBackground(Color.GREEN);
        lblh.setOpaque(true);
        this.add(lblh);
        
        lbll = new JLabel("Long: ", JLabel.TRAILING);
        lbll.setSize(50, 20);
        lbll.setLocation(5, 40);
        lbll.setBackground(Color.ORANGE);
        lbll.setOpaque(true);
        this.add(lbll);

        txth= new JTextField();
        txth.setSize(50, 20);
        txth.setLocation(60, 5);
        this.add(txth);
        
        txtl= new JTextField();
        txtl.setSize(50, 20);
        txtl.setLocation(60, 40);
        this.add(txtl);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 40);
        this.add(btnCalculate);

        lblResult = new JLabel("Rectangle Wide = ??? Long = ??? area = ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 100);
        lblResult.setLocation(0, 70);
        lblResult.setBackground(Color.PINK);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strWide = txth.getText();
                    String strLong = txtl.getText();
                    double W = Double.parseDouble(strWide);
                    double L = Double.parseDouble(strLong);
                    Rectangle rectangle = new Rectangle(W,L);
                    lblResult.setText("Rectangle Wide = "+ String.format("%.2f", rectangle.getW())
                            + " Rectangle Long = "+String.format("%.2f", rectangle.getL())  
                            + " area = " + String.format("%.2f", rectangle.calArea())
                            + " peimeter = " + String.format("%.2f", rectangle.calPerimter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(RectangleFrame2.this, "Error : Please input number", "Error "
                            , JOptionPane.ERROR_MESSAGE);
                    txth.setText("");
                    txth.requestFocus();
                    txtl.setText("");
                    txtl.requestFocus();
                }
            }

        });}
         public static void main(String[] args) {
        RectangleFrame2 frame = new RectangleFrame2();
        frame.setVisible(true);

    }

}
